<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#162946">
    <meta name="theme-color" content="#e67605">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"> <!-- Title -->
    <title>Car Listing,Dealer,Rental Auto Classifieds Bootstrap Modern Responsive Clean HTML Template</title>
    <!-- Bootstrap Css -->
    <base href="{{asset('admin')}}/">
    <link href="assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet"> <!-- Sidemenu Css -->
    <link href="assets/css/sidemenu.css" rel="stylesheet"> <!-- Switcher css -->
    <link href="assets/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all">
    <!-- Dashboard css -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/admin-custom.css" rel="stylesheet"> <!-- c3.js Charts Plugin -->
    <link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet">
    <!---Font icons-->
    <link href="assets/css/icons.css" rel="stylesheet"> <!-- Color-Skins -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="assets/colorskins/color-skins/color13.css">
    <link rel="stylesheet" href="assets/colorskins/demo.css">
  
    <meta http-equiv="imagetoolbar" content="no">
    <style type="text/css">
        <!-- input,textarea{-webkit-touch-callout:default;-webkit-user-select:auto;-khtml-user-select:auto;-moz-user-select:text;-ms-user-select:text;user-select:text} *{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:-moz-none;-ms-user-select:none;user-select:none} 
        -->
    </style>
    <style type="text/css" media="print">
        <!-- body{display:none} 
        -->
    </style>
    <!--[if gte IE 5]><frame></frame><![endif]-->
    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }

    </style>
</head>

<body class="construction-image">
   
    <div class="switcher-wrapper ">
        <div class="demo_changer">
            <div class="demo-icon bg_dark"><i class="fa fa-cog fa-spin  text_primary"></i></div>
            <div class="form_holder sidebar-right1">
                <div class="row">
                    <div class="predefined_styles">
                        <h4>Autolist Versions</h4>
                        <div class="swichermainleft p-4">
                            <div class="pl-3 pr-3"> <a href="../../index.html"
                                    class="btn btn-warning btn-block mt-0">LTR VERSION</a> <a href="../../index1.html"
                                    class="btn btn-success btn-block">RTL VERSION</a> </div>
                        </div>
                        <div class="swichermainleft border-top text-center p-4">
                            <div class="p-3"> <a href="../../index.html" class="btn btn-primary btn-block mt-0">View
                                    Demo</a> <a
                                    href="https://themeforest.net/item/autolist-car-dealer-and-classifieds-html-template/24416231"
                                    class="btn btn-secondary btn-block">Buy Now</a> <a
                                    href="https://themeforest.net/user/sprukosoft/portfolio"
                                    class="btn btn-info btn-block">Our Portfolio</a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End Switcher -->
    <!--Loader-->
    <div id="global-loader" style="display: none;"> <img src="assets/images/loader.svg" class="loader-img " alt="">
    </div>
    <!--/Loader-->
    <!--Page-->
    <div class="page page-h">
        <div class="page-content z-index-10">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-12 col-md-12 d-block mx-auto">
                        <div class="card box-shadow-0 mb-0">
                            <div class="card-header">
                                <h3 class="card-title">Đăng nhập</h3>
                            </div>
                            @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    {{$error}}<br>
                                @endforeach
                            </div>
                            @endif
                            @if(session('thongbao'))
                                <div class="alert alert-danger">{{session('thongbao')}}</div>
                            @endif
                        <form action="{{route('user.store')}}" method="post">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group"> <label class="form-label text-dark">Tài khoản</label>
                                        <input type="text" class="form-control" name="username" placeholder="Nhập tên tài khoản"> </div>
                                    <div class="form-group"> <label class="form-label text-dark">Mật khẩu</label> <input
                                            type="password" class="form-control" id="exampleInputPassword1"
                                            placeholder="Nhập mật khẩu" name="password"> </div>
                                    <div class="form-group"> <label class="custom-control custom-checkbox"> 
                                        <a href="forgot-password.html" class="float-right small text-dark mt-1">Quên mật khẩu</a>
                                         <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-label text-dark">Nhớ đăng nhập</span> </label></div>
                                    <div class="form-footer mt-2"> <input type="submit" value="Đăng nhập" class="btn btn-primary btn-block"></input></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/Page-->
    <!-- JQuery js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJtFydl4VJigO4llcmL");

        -->
    </script> <!-- Bootstrap js --> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("u");

        -->
    </script> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNQlmA\"msrleIInp :.jRve!bOi1");

        -->
    </script>
    <!--JQueryVehiclerkline Js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/js/vendors/jquery.sparkline.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJcvaDD0Cdyh\'uoOE<EedvtO");

        -->
    </script> <!-- Circle Progress Js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/js/vendors/circle-progress.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJ<<u\"jJhOeN\'wvOc6Pd");

        -->
    </script> <!-- Star Rating Js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/rating/jquery.rating-stars.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJiBiDRue\'SbviwB6");

        -->
    </script> <!-- P-scroll js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/p-scrollbar/p-scrollbar.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("u");

        -->
    </script> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/p-scrollbar/p-scroll1.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJNmOElE?/tv>JccbD<LQ4");

        -->
    </script> <!-- Fullside-menu Js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/toggle-sidebar/sidemenu.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNlVmFdzf6ttSNw");

        -->
    </script>
    <!--Counters --> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/counters/counterup.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("u");

        -->
    </script> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/plugins/counters/waypoints.min.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJcmid 6CcLSghl");

        -->
    </script> <!-- Custom Js--> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/js/admin-custom.js"></script>
    <script type="text/javascript">
        <!--
        lnzi("ud=xNJ<44d1irFSbvwwls<");

        -->
    </script> <!-- Switcher js --> <noscript>
        <p>To display this page you need a browser that supports JavaScript.</p>
    </noscript>
    <script src="assets/switcher/js/switcher.js"></script>
</body>

</html>
